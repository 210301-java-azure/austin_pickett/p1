# Fast Track

Fast Track is designed to be a light weight workout tracker. The goal for Fast Track is to provide a platform to easily and quickly log your workouts. 

## Technologies Used

    Java 8
    Hibernate 5.4.30
    JavaScript 

## Features

## List of features ready and TODOs for future development

    Login and Register
    Calander Date Selection 
    Add/Update/Remove workout items

## To-do list:

    Display items on calender
    Add support for other Browsers (current: FireFox)

## Usage

    http://fasttrack1.blob.core.windows.net/workout/home.html
    

## License

This project uses the following license: MIT License


