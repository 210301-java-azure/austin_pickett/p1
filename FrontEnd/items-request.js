// getting item data from our server
var items;
const xhr = new XMLHttpRequest(); // ready state 0 
xhr.open("GET", "http://localhost:8080/items"); // ready state 1
let authtoken = sessionStorage.getItem("token"); // we could set the Auth header with this value instead
xhr.setRequestHeader("Authorization", authtoken);
xhr.setRequestHeader("q-date",sessionStorage.getItem("q-date"));
xhr.onreadystatechange = function(){
    if(xhr.readyState == 4){
        if(xhr.status == 200){
            items = JSON.parse(xhr.responseText);
            renderItemsInTable(items);
        } else {
            console.log("something went wrong with your request")
        }
    }
}
xhr.send(); // ready state 2,3,4 follow 


function renderItemsInTable(itemsList){
    document.getElementById("items-table").hidden = false;
    const tableBody = document.getElementById("items-table-body");
    for(let item of itemsList){
        let newRow = document.createElement("tr");
        
        let selectBtn = document.createElement("td");        
        newRow.innerHTML = `<td>${item.id}</td><td ><a>${item.name}<a></td><td>${item.sets}</td><td>${item.reps}</td><td>${item.calories}</td>`;
        newRow.setAttribute("onclick","selectItem("+item.id+")");
        let newTd = document.createElement("td");
        let newBtn = document.createElement("button");
        newBtn.setAttribute("type","button");
        newBtn.setAttribute("onclick","deleteItem("+item.id+")");
        newBtn.setAttribute("class","close");
        newBtn.innerHTML=`<span aria-hidden="true">&times;</span>`;
        newTd.appendChild(newBtn );      
        newRow.appendChild(newTd);

        tableBody.appendChild(newRow);
    }    
}
function selectItem(i){
    for(let item of items){
        if(item.id==i){
           document.getElementById("item-id").value=item.id;
            document.getElementById("item-name").value=item.name;
            document.getElementById("item-sets").value=item.sets;
            document.getElementById("item-reps").value=item.reps;
            document.getElementById("item-calories").value=item.calories; 
            document.getElementById("item-date").value=item.date; 
        }
    }
    console.log("selected item");
    
    
}
