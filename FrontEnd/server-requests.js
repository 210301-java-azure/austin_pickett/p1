
function sendAjaxRequest(method, url, body, successCallback, failureCallback, authToken){
    const xhr = new XMLHttpRequest(); // ready state 0 
    xhr.open(method, url); // ready state 1
    if(authToken){
        xhr.setRequestHeader("Authorization", authToken);
    }
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status>199 && xhr.status<300){
                successCallback(xhr);
            } else {
                failureCallback(xhr);
            }
        }
    }
    if(body){
        xhr.send(body);
    } else {
        xhr.send(); // ready state 2,3,4 follow
    }
}

function sendAjaxPost(url, body, successCallback, failureCallback, authToken){
    sendAjaxRequest("POST", url, body, successCallback, failureCallback, authToken);
}

function ajaxLogin(username, password, successCallback, failureCallback){
    const payload = `username=${username}&password=${password}`; // "username="+username+"&password"+password
    sendAjaxPost("http://localhost:8080/login", payload, successCallback, failureCallback);
}
function ajaxRegister(username, password,email,firstname,lastname,auth, successCallback, failureCallback){
    const payload = `username=${username}&password=${password}&email=${email}&firstName=${firstname}&lastName=${lastname}&auth=${auth}`; // "username="+username+"&password"+password
    sendAjaxPost("http://localhost:8080/register", payload, successCallback, failureCallback);
}


function ajaxCreateItem(item, successCallback, failureCallback){
    console.log("ajaxCreateItem function");
    console.log(item);
    const itemJson = JSON.stringify(item);
    const auth = sessionStorage.getItem("token");
    sendAjaxPost("http://localhost:8080/items", itemJson, successCallback, failureCallback, auth);
}
function ajaxUpdateItem(item, successCallback, failureCallback){
    const itemJson = JSON.stringify(item);
    const auth = sessionStorage.getItem("token");
    sendAjaxRequest("PUT","http://localhost:8080/items/"+item.id, itemJson, successCallback, failureCallback, auth);
}
function ajaxDeleteItem(item, successCallback, failureCallback){
    console.log(item);
    const itemJson = JSON.stringify(item);
    const auth = sessionStorage.getItem("token");
    sendAjaxRequest("DELETE","http://localhost:8080/items/"+item, itemJson, successCallback, failureCallback, auth);
}
 
