document.getElementById("login-form").addEventListener("submit",attemptLogin);
document.getElementById("register-form").addEventListener("submit",attemptRegister);

function attemptLogin(event){
    event.preventDefault(); // this stops the form from sending a GET request by default to the same URL
    const username = document.getElementById("inputUsername").value;
    const password = document.getElementById("inputPassword").value;
    ajaxLogin(username, password, successfulLogin, loginFailed)
}
function successfulLogin(xhr){
    console.log("Succesful login"); 
    const authToken = xhr.getResponseHeader("Authorization");  
    sessionStorage.setItem("token", authToken);
    console.log(sessionStorage.getItem("token"));
    window.location.href = "./home.html";
}
function loginFailed(xhr){
    console.log("Failed login");
    const errorDiv = document.getElementById("error-msg");
    errorDiv.hidden = false;
    errorDiv.innerText = "Invalid User Credentials";
}

function attemptRegister(event){
    event.preventDefault(); // this stops the form from sending a GET request by default to the same URL
    const username = document.getElementById("rinputUsername").value;
    const password = document.getElementById("rinputPassword").value;
    const email = document.getElementById("rinputemail").value;
    const firstname = document.getElementById("rinputFirstName").value;
    const lastname= document.getElementById("rinputLastName").value;
    const auth = 2;

    ajaxRegister(username, password,email,firstname,lastname,auth, successfulRegister, registerFailed)
}
function successfulRegister(xhr){
    console.log("Succesful register"); 
    const authToken = xhr.getResponseHeader("Authorization");  
    sessionStorage.setItem("token", authToken);
    console.log(sessionStorage.getItem("token"));
    window.location.href = "./home.html";
}
function registerFailed(xhr){
    console.log("Failed registration");
    const errorDiv = document.getElementById("error-msg2");
    errorDiv.hidden = false;
    errorDiv.innerText = "Username is already in use";
}

