document.getElementById("add-item").addEventListener("click", addNewItem);
document.getElementById("update-item").addEventListener("click", updateItem);

function addNewItem(e){
    e.preventDefault();
    
    const itemName = document.getElementById("item-name").value;
    const itemSets = document.getElementById("item-sets").value;
    const itemReps = document.getElementById("item-reps").value;
    const itemCal = document.getElementById("item-calories").value;
    const itemDate = sessionStorage.getItem("q-date");
    const newItem = {"name": itemName, "sets": itemSets,"reps": itemReps,"calories": itemCal,"date":itemDate};
    // javascript object (newItem) -> json -> java object (MarketItem.class)
    console.log("got this far?");
    ajaxCreateItem(newItem, indicateSuccess, indicateFailure);
}

function indicateSuccess(){
    location.reload(); 
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "New item successfully created";
}

function indicateFailure(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "There was an issue creating your new item";
}

function updateItem(e){
    e.preventDefault();
    const itemId = document.getElementById("item-id").value;
    const itemName = document.getElementById("item-name").value;
    const itemSets = document.getElementById("item-sets").value;
    const itemReps = document.getElementById("item-reps").value;
    const itemCal = document.getElementById("item-calories").value;
    const itemDate = sessionStorage.getItem("q-date");
    const newItem = {"id":itemId,"name": itemName, "sets": itemSets,"reps": itemReps,"calories": itemCal,"date":itemDate};
    // javascript object (newItem) -> json -> java object (MarketItem.class)
    
    ajaxUpdateItem(newItem, indicateUpdateSuccess, indicateUpdateFailure);
}

function indicateUpdateSuccess(){
    location.reload(); 
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "Item successfully updated";
}

function indicateUpdateFailure(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "Item failed to update";
}

function deleteItem(e){    
    ajaxDeleteItem(e, indicateDeleteSuccess, indicateDeleteFailure);
}

function indicateDeleteSuccess(){
    location.reload(); 
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "Item successfully deleted";
}

function indicateDeleteFailure(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "Item faild to delete";
}
