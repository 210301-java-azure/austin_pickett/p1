package dev.pickett.models;



import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@JsonIgnoreProperties("hibernateLazyInitializer")
public class WorkoutItem implements Serializable {
//make empty user() then set id for saving and loading from database
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "calories")
    private double calories;
    @Column(name = "name")
    private String name;
    @Column(name = "sets")
    private int sets;
    @Column(name = "reps")
    private int reps;

    @ManyToOne
    private User user;

    @Column(name = "date")
    private LocalDate date;

    public WorkoutItem(){

    }
    public WorkoutItem(int id){

    }

    public WorkoutItem(int id, double calories, String name, int sets, int reps,User user, LocalDate date) {
        this.id = id;
        this.calories = calories;
        this.name = name;
        this.sets = sets;
        this.user = user;
        this.reps = reps;
        this.date = date;
    }


    public int getId() {
        return id;
    }

    public double getCalories() {
        return calories;
    }

    public void setCalories(double calories) {
        this.calories = calories;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSets() {
        return sets;
    }

    public void setSets(int sets) {
        this.sets = sets;
    }

    public int getReps() {
        return reps;
    }

    public void setReps(int reps) {
        this.reps = reps;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "WorkoutItem{" +
                "id=" + id +
                ", calories=" + calories +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WorkoutItem that = (WorkoutItem) o;
        return id == that.id && Double.compare(that.calories, calories) == 0 && sets == that.sets && reps == that.reps && user == that.user && Objects.equals(name, that.name) && Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, calories, name, sets, reps, user, date);
    }
}
