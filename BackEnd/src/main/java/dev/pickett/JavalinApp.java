package dev.pickett;

import dev.pickett.controllers.AuthController;
import dev.pickett.controllers.UserController;
import dev.pickett.controllers.WorkoutItemController;
import dev.pickett.util.SecurityUtil;
import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;

import static io.javalin.apibuilder.ApiBuilder.*;
import static io.javalin.apibuilder.ApiBuilder.put;

public class JavalinApp {

    WorkoutItemController wic = new WorkoutItemController();
    UserController uc = new UserController();
    AuthController auth = new AuthController();
    SecurityUtil secUtil = new SecurityUtil();


        Javalin app = Javalin.create(JavalinConfig::enableCorsForAllOrigins).routes(() -> {
        before("items*",auth::authorizeToken);
        path("items", () -> {
            get(wic::handleGetAllItems);
            post(wic::handlePostItem);
            path(":id", () -> {
                get(wic::handleGetItemById);
                delete(wic::handleDeleteItem);
                put(wic::handlePutItem);
            });
        });
        path("login", () -> {
            post(auth::authenticateLogin);
            after("/",secUtil::attachResponseHeaders);

        });
        path("register", () -> {
            post(uc::handlePostUser);
            after("/",secUtil::attachResponseHeaders);
        });
        path("users", () -> {
            //look into an access manager
            get(uc::handleGetAllUsers);
            path(":id", () -> {
                get(uc::handleGetUserById);
                delete(uc::handleDeleteUser);
                put(uc::handlePutUser);
            });
        });

    });
    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }


}
