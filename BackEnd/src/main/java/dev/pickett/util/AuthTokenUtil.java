package dev.pickett.util;
import com.auth0.jwt.*;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.interfaces.RSAPublicKey;

public class AuthTokenUtil {

    private final Logger logger = LoggerFactory.getLogger(AuthTokenUtil.class);
    private static JWTVerifier verifier;

    public static String getAuthToken(String kid){
        try{
            //Algorithm algorithm = Algorithm.HMAC256(System.getenv("secret"));
            Algorithm algorithm = Algorithm.HMAC256("thissecret");
            String token = JWT.create()
                    .withIssuer("auth0")
                    .withSubject(kid)
                    .sign(algorithm);
            return token;
        }catch(JWTCreationException e){

        }
        return null;
    }
    public static int verifyAuthToken(String token){
        if(verifier==null){
           // Algorithm algorithm = Algorithm.HMAC256(System.getenv("this"));
            Algorithm algorithm = Algorithm.HMAC256("thissecret");//hard coded this to get jenkins going
           verifier = JWT.require(algorithm)
                    .withIssuer("auth0")
                    .build();
        }
        try{

            DecodedJWT jwt = verifier.verify(token);

            return Integer.parseInt(jwt.getSubject());
        }catch(JWTCreationException e){

        }

        return -1;
    }
}
