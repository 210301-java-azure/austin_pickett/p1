package dev.pickett.util;

import dev.pickett.models.User;
import dev.pickett.models.WorkoutItem;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import java.util.Properties;

public class HibernateUtil {


    private static SessionFactory sessionFactory;

    private static SessionFactory getSessionFactory(){
        if(sessionFactory == null){
            Configuration configuration = new Configuration();
            Properties settings = new Properties();
          // settings.put(Environment.URL,"jdbc:postgresql://localhost:5432/postgres");
            settings.put(Environment.URL,"jdbc:sqlserver://mangabuffet.database.windows.net:1433;database=MangaApp_db;");
            //settings.put(Environment.URL,System.getenv("connectionUrl"));
            settings.put(Environment.USER,"austinspickett@mangabuffet");
            settings.put(Environment.PASS,"Rev.pass");

            settings.put(Environment.DRIVER, "com.microsoft.sqlserver.jdbc.SQLServerDriver");
            //https://www.javatpoint.com/dialects-in-hibernate
            settings.put(Environment.DIALECT, "org.hibernate.dialect.SQLServerDialect");


            //settings.put(Environment.DRIVER,"org.postgresql.Driver");
            //settings.put(Environment.DIALECT,"org.hibernate.dialect.PostgreSQLDialect");

            settings.put(Environment.HBM2DDL_AUTO,"update");//create here to create tables or use validate
            settings.put(Environment.SHOW_SQL,"true");


            configuration.setProperties(settings);
            configuration.addAnnotatedClass(WorkoutItem.class);
            configuration.addAnnotatedClass(User.class);

            sessionFactory = configuration.buildSessionFactory();

        }
        return sessionFactory;
    }

    public static Session getSession(){
        return getSessionFactory().openSession();
    }
}
